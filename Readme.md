# Variable Extractor Tasks for Bamboo

A few Bamboo tasks to help extracting bamboo result variables from files in the build directory.
User documentation can be found in the [wiki](https://bitbucket.org/illucit/bamboo-variables-extractor-plugin/wiki/Home).

## Building and Developing

To build it:

    mvn install
