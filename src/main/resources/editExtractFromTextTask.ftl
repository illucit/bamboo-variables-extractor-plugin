[@ui.bambooSection titleKey="com.illucit.bamboo.plugins.varextract.variable.title"]
    [@ww.textfield labelKey="com.illucit.bamboo.plugins.varextract.variable.name" name="variable"
    required='true' cssClass="long-field"/]
    [@ww.checkbox labelKey="com.illucit.bamboo.plugins.varextract.variable.failifmissing" name="failIfMissing" /]
    [@ww.textfield labelKey="com.illucit.bamboo.plugins.varextract.variable.default" name="defaultValue"
    required='false' cssClass="long-field"/]
[/@ui.bambooSection]

[@ui.bambooSection titleKey="com.illucit.bamboo.plugins.varextract.file.title"]
    [@ww.textfield labelKey="com.illucit.bamboo.plugins.varextract.file.filename" name="filename" cssClass="long-field" required='true'/]
    [@ww.textfield labelKey="com.illucit.bamboo.plugins.varextract.file.expression" name="expression" cssClass="long-field" required='true'/]
[/@ui.bambooSection]
