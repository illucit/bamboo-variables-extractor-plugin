package com.illucit.bamboo.plugins.varextract.configuration;

import static com.illucit.bamboo.plugins.varextract.AbstractExtractTask.CONFIG_KEY_DEFAULT_VALUE;
import static com.illucit.bamboo.plugins.varextract.AbstractExtractTask.CONFIG_KEY_FAIL_IF_MISSING;
import static com.illucit.bamboo.plugins.varextract.AbstractExtractTask.CONFIG_KEY_FILENAME;
import static com.illucit.bamboo.plugins.varextract.AbstractExtractTask.CONFIG_KEY_VARIABLE;

import java.util.Map;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;

public abstract class AbstractExtractConfigurator extends AbstractTaskConfigurator {

	@Override
	public Map<String, String> generateTaskConfigMap(ActionParametersMap params, TaskDefinition previousTaskDefinition) {
		Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
		config.put(CONFIG_KEY_VARIABLE, params.getString(CONFIG_KEY_VARIABLE));
		config.put(CONFIG_KEY_FILENAME, params.getString(CONFIG_KEY_FILENAME));
		config.put(CONFIG_KEY_FAIL_IF_MISSING, params.getString(CONFIG_KEY_FAIL_IF_MISSING));
		config.put(CONFIG_KEY_DEFAULT_VALUE, params.getString(CONFIG_KEY_DEFAULT_VALUE));
		return config;
	}

	@Override
	public void populateContextForCreate(Map<String, Object> context) {
		super.populateContextForCreate(context);
		context.put(CONFIG_KEY_VARIABLE, "");
		context.put(CONFIG_KEY_FILENAME, "");
		context.put(CONFIG_KEY_FAIL_IF_MISSING, true);
		context.put(CONFIG_KEY_DEFAULT_VALUE, "");
	}

	@Override
	public void populateContextForEdit(Map<String, Object> context, TaskDefinition taskDefinition) {
		super.populateContextForEdit(context, taskDefinition);
		context.put(CONFIG_KEY_VARIABLE, taskDefinition.getConfiguration().get(CONFIG_KEY_VARIABLE));
		context.put(CONFIG_KEY_FILENAME, taskDefinition.getConfiguration().get(CONFIG_KEY_FILENAME));
		context.put(CONFIG_KEY_FAIL_IF_MISSING, taskDefinition.getConfiguration().get(CONFIG_KEY_FAIL_IF_MISSING));
		context.put(CONFIG_KEY_DEFAULT_VALUE, taskDefinition.getConfiguration().get(CONFIG_KEY_DEFAULT_VALUE));
	}

	@Override
	public void populateContextForView(Map<String, Object> context, TaskDefinition taskDefinition) {
		super.populateContextForView(context, taskDefinition);
		context.put(CONFIG_KEY_VARIABLE, taskDefinition.getConfiguration().get(CONFIG_KEY_VARIABLE));
		context.put(CONFIG_KEY_FILENAME, taskDefinition.getConfiguration().get(CONFIG_KEY_FILENAME));
		context.put(CONFIG_KEY_FAIL_IF_MISSING, taskDefinition.getConfiguration().get(CONFIG_KEY_FAIL_IF_MISSING));
		context.put(CONFIG_KEY_DEFAULT_VALUE, taskDefinition.getConfiguration().get(CONFIG_KEY_DEFAULT_VALUE));
	}

	@Override
	public void validate(ActionParametersMap params, ErrorCollection errorCollection) {
		super.validate(params, errorCollection);

		if (params.getString(CONFIG_KEY_VARIABLE) == null || params.getString(CONFIG_KEY_VARIABLE).isEmpty()) {
			errorCollection.addError(CONFIG_KEY_VARIABLE,
					getI18nBean().getText("com.illucit.bamboo.plugins.varextract.empty.error"));
		}
		if (params.getString(CONFIG_KEY_FILENAME) == null || params.getString(CONFIG_KEY_FILENAME).isEmpty()) {
			errorCollection.addError(CONFIG_KEY_FILENAME,
					getI18nBean().getText("com.illucit.bamboo.plugins.varextract.empty.error"));
		}
	}

}
