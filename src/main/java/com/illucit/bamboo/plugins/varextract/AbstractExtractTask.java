package com.illucit.bamboo.plugins.varextract;

import java.io.File;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.deployments.execution.DeploymentTaskContext;
import com.atlassian.bamboo.deployments.execution.DeploymentTaskType;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import com.atlassian.bamboo.variable.VariableContext;
import com.atlassian.bamboo.variable.VariableDefinitionContext;

public abstract class AbstractExtractTask implements TaskType, DeploymentTaskType {

	public static final String CONFIG_KEY_VARIABLE = "variable";
	public static final String CONFIG_KEY_FAIL_IF_MISSING = "failIfMissing";
	public static final String CONFIG_KEY_DEFAULT_VALUE = "defaultValue";
	public static final String CONFIG_KEY_FILENAME = "filename";

	@Override
	public TaskResult execute(TaskContext taskContext) throws TaskException {
		return executeTask(taskContext);
	}

	@Override
	public TaskResult execute(DeploymentTaskContext taskContext) throws TaskException {
		return executeTask(taskContext);
	}

	private TaskResult executeTask(CommonTaskContext taskContext) throws TaskException {
		ConfigurationMap config = taskContext.getConfigurationMap();
		String variableName = config.get(CONFIG_KEY_VARIABLE);
		String fileName = config.get(CONFIG_KEY_FILENAME);
		boolean failIfMissing = Boolean.parseBoolean(config.get(CONFIG_KEY_FAIL_IF_MISSING));
		String defaultValue = config.get(CONFIG_KEY_DEFAULT_VALUE);
		if (defaultValue == null) {
			defaultValue = "";
		}

		BuildLogger buildLogger = taskContext.getBuildLogger();

		String variableValue = null;
		File inputFile = new File(taskContext.getWorkingDirectory(), fileName);
		if (!inputFile.exists()) {
			if (failIfMissing) {
				throw new TaskException(String.format(
						"File not found for variable extraction: %s (in build directory %s)", fileName, taskContext
								.getWorkingDirectory().getAbsolutePath()));
			}
			buildLogger.addBuildLogEntry(String.format(
					"File not found for variable extraction: %s (in build directory %s). "
							+ "Using default value instead.", fileName, taskContext.getWorkingDirectory()
							.getAbsolutePath()));
		} else {
			try {
				variableValue = readVariableFromFile(inputFile, taskContext, buildLogger);
			} catch (TaskException e) {
				String msg = e.getMessage();
				if (failIfMissing) {
					throw new TaskException(String.format("Error extraction variable from file %s: %s",
							inputFile.getAbsolutePath(), msg));
				}
				buildLogger.addBuildLogEntry(String.format("Error extraction variable from file %s: %s. "
						+ "Using default value instead.", inputFile.getAbsolutePath(), msg));
			}
		}
		if (variableValue == null) {
			variableValue = defaultValue;
		}
		buildLogger.addBuildLogEntry("Variable " + variableName + " was extracted with value '" + variableValue + "'.");

		VariableContext variableContext = taskContext.getCommonContext().getVariableContext();
		saveResultVariable(variableName, variableValue, buildLogger, variableContext);

		return TaskResultBuilder.newBuilder(taskContext).success().build();
	}

	private void saveResultVariable(final String variableName, String newValue, BuildLogger buildLogger,
			VariableContext variableContext) {
		String oldValue = null;
		for (VariableDefinitionContext varCtx : variableContext.getEffectiveVariables().values()) {
			if (variableName.equals(varCtx.getKey())) {
				oldValue = varCtx.getValue();
				break;
			}
		}
		variableContext.addResultVariable(variableName, newValue);
		buildLogger.addBuildLogEntry(String.format("Changing variable %s of type RESULT from %s to %s", variableName,
				oldValue, newValue));

	}

	/**
	 * Try to read variable value from file.
	 * 
	 * @param file
	 *            file to extract the value from
	 * @param taskContext
	 *            the task context
	 * @param buildLogger
	 *            the build logger
	 * @return an extracted value
	 * @throws TaskException
	 *             if the value can not be extracted from the file
	 */
	protected abstract String readVariableFromFile(File file, CommonTaskContext taskContext, BuildLogger buildLogger)
			throws TaskException;

}
