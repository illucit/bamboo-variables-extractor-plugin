package com.illucit.bamboo.plugins.varextract;

import static org.apache.commons.io.Charsets.UTF_8;
import static org.apache.commons.io.FileUtils.readFileToString;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskException;

public class ExtractFromTextTask extends AbstractExtractTask {

	public static final String CONFIG_KEY_EXPRESSION = "expression";
	
	@Override
	protected String readVariableFromFile(File file, CommonTaskContext taskContext, BuildLogger buildLogger)
			throws TaskException {
		try {
			String expression = taskContext.getConfigurationMap().get(CONFIG_KEY_EXPRESSION);
			String fileContent = readFileToString(file, UTF_8);
			Pattern pattern = Pattern.compile(expression);
			Matcher matcher = pattern.matcher(fileContent);
			if (!matcher.find()) {
				throw new TaskException("Pattern did not match");
			}
			if (matcher.groupCount() != 1) {
				throw new TaskException("Pattern requires exactly 1 group to match");
			}
			return matcher.group(1);
		} catch (TaskException e) {
			// Let exception be handled by AbstractExtractTask
			throw e;
		} catch (Exception e) {
			buildLogger.addErrorLogEntry("Error extracting variable", e);
			throw new TaskException("Error extracting variable: " + e.getMessage());
		}
	}
	
}
