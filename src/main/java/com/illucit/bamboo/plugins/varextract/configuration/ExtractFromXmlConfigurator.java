package com.illucit.bamboo.plugins.varextract.configuration;

import static com.illucit.bamboo.plugins.varextract.ExtractFromXmlTask.CONFIG_KEY_XPATH;

import java.util.Map;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;

public class ExtractFromXmlConfigurator extends AbstractExtractConfigurator {

	@Override
	public Map<String, String> generateTaskConfigMap(ActionParametersMap params, TaskDefinition previousTaskDefinition) {
		Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
		config.put(CONFIG_KEY_XPATH, params.getString(CONFIG_KEY_XPATH));
		return config;
	}

	@Override
	public void populateContextForCreate(Map<String, Object> context) {
		super.populateContextForCreate(context);
		context.put(CONFIG_KEY_XPATH, "//MyTag/MySubTag");
	}

	@Override
	public void populateContextForEdit(Map<String, Object> context, TaskDefinition taskDefinition) {
		super.populateContextForEdit(context, taskDefinition);
		context.put(CONFIG_KEY_XPATH, taskDefinition.getConfiguration().get(CONFIG_KEY_XPATH));
	}

	@Override
	public void populateContextForView(Map<String, Object> context, TaskDefinition taskDefinition) {
		super.populateContextForView(context, taskDefinition);
		context.put(CONFIG_KEY_XPATH, taskDefinition.getConfiguration().get(CONFIG_KEY_XPATH));
	}

	@Override
	public void validate(ActionParametersMap params, ErrorCollection errorCollection) {
		super.validate(params, errorCollection);

		if (params.getString(CONFIG_KEY_XPATH) == null || params.getString(CONFIG_KEY_XPATH).isEmpty()) {
			errorCollection.addError(CONFIG_KEY_XPATH,
					getI18nBean().getText("com.illucit.bamboo.plugins.varextract.empty.error"));
		}
	}
	
}
