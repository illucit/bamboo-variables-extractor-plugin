package com.illucit.bamboo.plugins.varextract;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskException;

public class ExtractFromXmlTask extends AbstractExtractTask {

	public static final String CONFIG_KEY_XPATH = "xpath";

	@Override
	protected String readVariableFromFile(File file, CommonTaskContext taskContext, BuildLogger buildLogger)
			throws TaskException {
		InputStream inStream = null;
		try {
			String xpathQuery = taskContext.getConfigurationMap().get(CONFIG_KEY_XPATH);

			DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();
			dfactory.setNamespaceAware(false);
			DocumentBuilder docBuilder = dfactory.newDocumentBuilder();
			inStream = new FileInputStream(file);
			Document doc = docBuilder.parse(inStream);

			XPath xpath = XPathFactory.newInstance().newXPath();
			XPathExpression expr = xpath.compile(xpathQuery);
			Object result = expr.evaluate(doc, XPathConstants.STRING);
			if (result instanceof String) {
				return (String) result;
			}
			throw new TaskException("XPath returned no String");
		} catch (TaskException e) {
			// Let exception be handled by AbstractExtractTask
			throw e;
		} catch (Exception e) {
			buildLogger.addErrorLogEntry("Error extracting variable", e);
			throw new TaskException("Error extracting variable: " + e.getMessage());
		} finally {
			if (inStream != null) {
				try {
					inStream.close();
				} catch (IOException e) {
					// NOOP
				}
			}
		}
	}

}
