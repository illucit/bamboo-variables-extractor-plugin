<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

	<parent>
		<groupId>com.atlassian.pom</groupId>
		<artifactId>atlassian-public-pom</artifactId>
		<version>24</version>
	</parent>

	<modelVersion>4.0.0</modelVersion>
	<groupId>com.illucit</groupId>
	<artifactId>bamboo-variables-extractor-plugin</artifactId>
	<version>1.30.0</version>

	<name>Variable Extractor Tasks for Bamboo</name>
	<description>Extract Bamboo result variables from XML or text files in your project.</description>
	<packaging>atlassian-plugin</packaging>

	<organization>
		<name>illucIT Software GmbH</name>
		<url>https://www.illucit.com</url>
	</organization>

	<licenses>
		<license>
			<name>Apache License, Version 2.0</name>
			<distribution>repo</distribution>
			<url>http://www.apache.org/licenses/LICENSE-2.0.html</url>
		</license>
	</licenses>

	<developers>
		<developer>
			<id>csimon</id>
			<name>Christian Simon</name>
			<email>simon@illucit.com</email>
			<url>https://github.com/metaxmx</url>
		</developer>
	</developers>

	<scm>
		<connection>scm:git:${scm.connection}</connection>
		<developerConnection>scm:git:${scm.connection}</developerConnection>
		<url>${scm.url}</url>
		<tag>HEAD</tag>
	</scm>

	<properties>
		<!-- Release Info -->
		<scm.connection>git@bitbucket.org:illucit/bamboo-variables-extractor-plugin.git</scm.connection>
		<scm.url>https://bitbucket.org/illucit/bamboo-variables-extractor-plugin</scm.url>

		<!-- API Versions -->
		<bamboo.version>5.7.0</bamboo.version>
		<bamboo.data.version>3.2.2</bamboo.data.version>
		<amps.version>6.3.7</amps.version>
		<failOnMilestoneOrReleaseCandidateDeps>false</failOnMilestoneOrReleaseCandidateDeps>

		<!-- Atlassian Plugin Definitions -->
		<bamboo.minsupported>${bamboo.version}</bamboo.minsupported>
		<bamboo.maxsupported>9.0.0</bamboo.maxsupported>
	</properties>

	<dependencies>
		<dependency>
			<groupId>com.atlassian.bamboo</groupId>
			<artifactId>atlassian-bamboo-web</artifactId>
			<version>${bamboo.version}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.atlassian.bamboo</groupId>
			<artifactId>atlassian-bamboo-deployments</artifactId>
			<version>${bamboo.version}</version>
			<scope>provided</scope>
		</dependency>
	</dependencies>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>com.atlassian.plugins</groupId>
				<artifactId>atlassian-plugins-core</artifactId>
				<version>3.0.16</version>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<build>
		<plugins>
			<plugin>
				<groupId>com.atlassian.maven.plugins</groupId>
				<artifactId>maven-bamboo-plugin</artifactId>
				<version>${amps.version}</version>
				<extensions>true</extensions>
				<configuration>
					<productVersion>${bamboo.version}</productVersion>
					<productDataVersion>${bamboo.data.version}</productDataVersion>
					<productDataPath>${project.build.directory}/bamboo-home</productDataPath>
				</configuration>
			</plugin>
			<plugin>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<source>1.6</source>
					<target>1.6</target>
				</configuration>
			</plugin>
		</plugins>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>com.atlassian.maven.plugins</groupId>
					<artifactId>maven-amps-plugin</artifactId>
					<version>${amps.version}</version>
				</plugin>
				<plugin>
					<groupId>com.atlassian.maven.plugins</groupId>
					<artifactId>maven-amps-dispatcher-plugin</artifactId>
					<version>${amps.version}</version>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>

	<repositories>
		<repository>
			<id>atlassian-public</id>
			<url>https://maven.atlassian.com/repository/public</url>
			<snapshots>
				<enabled>true</enabled>
				<updatePolicy>never</updatePolicy>
				<checksumPolicy>warn</checksumPolicy>
			</snapshots>
			<releases>
				<enabled>true</enabled>
				<checksumPolicy>warn</checksumPolicy>
			</releases>
		</repository>
	</repositories>

	<pluginRepositories>
		<pluginRepository>
			<id>atlassian-public</id>
			<url>https://maven.atlassian.com/repository/public</url>
			<releases>
				<enabled>true</enabled>
				<checksumPolicy>warn</checksumPolicy>
			</releases>
			<snapshots>
				<updatePolicy>never</updatePolicy>
				<checksumPolicy>warn</checksumPolicy>
			</snapshots>
		</pluginRepository>
	</pluginRepositories>

</project>
